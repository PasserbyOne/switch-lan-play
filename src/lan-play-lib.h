#ifndef _LAN_PLAY_LIB_H_
#define _LAN_PLAY_LIB_H_

typedef void (*llog_message)(const char *message);

int run_lan_play(int argc, char **argv);

void set_llog_message_fun(llog_message fun);

#endif