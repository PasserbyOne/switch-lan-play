#include<stdio.h>
#include "lan-play-lib.h"

void print_message(const char *msg) {
    printf("demo print message ==> %s", msg);
}

int main(int argc, char *argv[]) {
    set_llog_message_fun(print_message);
    run_lan_play(argc, argv);
}