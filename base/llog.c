#include <string.h>
#include <base/llog.h>

bool is_lan_play_lib = false;
llog_fun llog_fun_cb = NULL;

static char *llog_level_names[] = {NULL, "ERROR", "WARNING", "NOTICE", "INFO", "DEBUG"};

void LLog_log(int level, const char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);

    if (level <= LLOG_DISPLAY_LEVEL) {
        if (!is_lan_play_lib) {
            fprintf(stderr, "[%s]: ", llog_level_names[level]);
            vfprintf(stderr, fmt, ap);
            fprintf(stderr, "\n");
        } else {
            if (llog_fun_cb) {
                char message[2048];
                memset(message, 0, 2048);
                int count1 = sprintf(message, "[%s]: ", llog_level_names[level]);
                int count2 = vsprintf(message + count1, fmt, ap);
                sprintf(message + count1 + count2, "\n");
                llog_fun_cb(message);
            }
        }
    }

    va_end(ap);
}
